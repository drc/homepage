menu: Aktuelles
group: index
order: 999
template: page.html


Aktuelles
---------

Update 2021-08-10: Tickets stornieren
=====================================

Falls Du dieses Jahr nicht am HOA teilnehmen kannst oder willst kannst du
dein Camping-Ticket noch bis zum 22.08.2021 stornieren und bekommst den
gesamten Ticketpreis zurück.
Jedes zurückgegebene Ticket wird danach an Personen in der Warteschlange
weitergegeben.

Update vom 2021-08-02: Tickets für das HOA 2021
===============================================

Die Arbeit für das Hygienekonzept ist aktuell in vollem Gange.
Daher wissen wir aktuell noch nicht wie viele Tickets wir unter diesen
besonderen Bedingungen herausgeben können.
Falls Du noch ein Ticket für das HOA 2021 kaufen möchtest schreib dich
in unserem `Pretix <https://tickets.hackenopenair.de>`__ auf die Warteliste.
Sobald Plätze frei werden verteilen wir diese in der Reihenfolge der
Eintragung auf der Warteliste.

Update vom 2021-07-28: We are GO for HOA 2021
=============================================

Das HOA 2021 rückt näher: In nur sechs Wochen ist es schon so weit.
Um euch und uns mehr Planungssicherheit zu geben, haben wir uns noch ein letztes Mal gefragt:
Können und wollen wir das HOA dieses Jahr durchführen (\*0) ?

Diese Frage ist nicht einfach: Natürlich sehnen wir uns nach mehr Normalität und
auch für uns gehören Veranstaltungen wie das HOA zu dieser Normalität.
Aber natürlich wollen wir das HOA nur durchführen, wenn wir das corona-bedingte
gesundheitliche Risiko für euch und für uns für beherrschbar halten.

Unsere Antwort auf diese Fragen ist: Das HOA wird stattfinden (\*0), aber es wird ein paar
Besonderheiten geben.
Unser Hygiene-Rahmenkonzept sieht zur Zeit folgende Punkte vor:

* Impfung:
  Bei uns im Orga-Team haben wir festgestellt: Wir erreichen bis zum HOA eine Impfquote von
  nahezu 100%.
  Vermutlich wird das im restlichen Chaos nicht viel anders aussehen.
  Für alle, bei denen keine Gründe (medizinische, Alter, fehlende Impfempfehlung)
  gegen eine Impfung sprechen, sollten inzwischen ausreichend Impfangebote vorhanden sein.
  (Für die Verpeiler: Es ist sogar gerade noch genug Zeit! ;-)
  Wir denken daher, dass es im Sinne der Solidarität gegenüber allen Mit-Entitäten ist, sich (sofern möglich) impfen zu lassen.

  Von einer hohen Impfquote versprechen wir uns zwei Effekte: Infizierte sind weniger ansteckend und
  falls wir doch Neuinfektionen auf der Veranstaltung haben, sind die gesundheitlichen Auswirkungen weniger
  schwerwiegend.
  Um uns in unserer Annahme zu bestätigen wird es für jedes Ticket eine Umfrage im Pretix geben.
  Wir möchten alle bitten, daran teilzunehmen.
  Deadline für die Umfrage ist der 08.08.2021.

* Kein zentrales Hackcenter:
  Im Hackcenter werden die Köpfe nah zusammen gesteckt, um gemeinsam spannende Probleme zu lösen.
  Wir haben uns entschlossen, dass wir in diesem Jahr kein zentrales Hackcenter anbieten wollen.
  Bringt also selber Pavillione+Sitzgelegentheiten mit und bildet dezentrale Mini-Hackcenter.

* Weitere Maßnahmen:
  Darüber hinaus gehen wir davon aus, dass es weitere Maßnahmen geben wird.
  Neben zusätzlichen (Outdoor-) Duschen und mehr DIXIs können es aber auch eine Maskenpflicht in
  bestimmten Situationen (z.B. in unserem festen Haus oder bei der Essensausgabe),
  tägliche Antigen-Schnelltests für alle teilnehmenden (auch geimpfte) Entitäten
  oder ähnliches sein.


Wir haben die Rückgabefrist für Tickets bis zum 22.08.2021 verlängert.
Solltest Du doch nicht am HOA 2021 teilnehmen können oder wollen, kannst Du dein Ticket im Pretix
stornieren.

Beim Ticketkauf hast du unter Umständen Angaben zur An- und Abreise gemacht.
Hilf uns mit der Planung, indem Du diese aktualisierst.
Du findest all diese Fragen, auf der Detail-Seite deines Tickets.
Den Link dorthin haben wir allen Tickets noch einmal per E-Mail geschickt.


(\*0): Es gibt einen Niedersächsischen Stufenplan, der ab einer gewissen Inzidenz gewisse
Veranstaltungen verbietet.
Sollten die Inzidenz so weit steigen, dass wir davon betroffen sind müssen wir das HOA natürlich
trotzdem absagen.
In diesem Fall werden wir alle Tickets stornieren.
Eure T-Shirts bekommt ihr in diesem Fall auf einem anderen Weg, z.B. per Post.
