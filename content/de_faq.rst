menu: FAQ
group: index
order: 1000
template: page.html



Frequently Asked Questions
==========================

* | **Ich habe ein T-Shirt bestellt. Könnt ihr mir das per Post zusenden?**
  | Wir können deinen Merch nicht per Paket oder Post versenden. Falls Du zwar Merch bestellt hast aber nicht vor Ort bist bitte einen bekannten Hacker das Merch für dich abzuholen.
