menu: Infos 
group: en 
order: 10
template: base.html


Infos
=====

Deadlines
---------

T-Shirts
  Orders must be placed and paid until 2021-08-20 18:00 (UTC+2).

Cancel Tickets
  If you want to cancel your ticket you have to do so until
  2021-08-22 23:59 (UTC+2).
  Cancellation is free of charge.

Buy Tickets
  There is currently a waiting list.
  If you want to buy a ticket add yourself to the queue.

Question? Feel fee to ask: kontakt@hackenopenair.de

Tickets
-------

Tickets from HOA 2::20 are valid this year.
Currently there is a waiting list for tickets.
Because of Corona we don't know yet if there will be another
ticket sale.

If you can not or do not want to attend HOA this year you can cancel your
camping-ticket and get a 100% refund. This will be possible until 2021-08-22.
All tickets returned will be given to the people on the waiting list.


* Cool location for hacking and chilling
* Opportunity to give or attend a lecture or workshop
* Extensive breakfast from our buffet
* Warm dinner
* WiFi, Ethernet and DECT on the campground
* Camping with your tent including electricity, warm shower and sanitary
  facilities
  
Additionally there are discounted tickets for young Hackers from 12
and up to 16 years.

Admission for youngest hackers is free.

There will be no day tickets this year.

If you want to camp in your vehicle, camper or caravan you will need an
additional vehicle ticket.
Note: The number of vehicle tickets is limited.

T-Shirts are available in our presale.
You can pickup your T-Shirts during the event.
For a cosy hacking-weekend we will provide you with some infrastructure.
This section gives you an overview on what we are planning.

Hygiene concept
---------------

We want you to feel safe at HOA.
Corona will make this a special challenge this year.
We are currently developing a hygiene concept.
In case you would like to take a look at this document now
you can download it `here </hygienekonzept.pdf>`__
(German only, sorry!).
If you have any feedback on this, you can reach us at *kontakt@hackenopenair.de*.

In a few weeks there will also be a summary for visitors.

Parking
-------

We only have a few parking spaces on our campgroud.
But we are currently checking some possibilities.
We will probably have to fall back to parking spaces in Gifhorn.

Arrival and Departure
---------------------

Regular Arrival for camping will be stating at Thursday midday.
Regular Departure for camping will be starting Sunday afternoon.

If you want to help us build up you can arrive starting from Wednesday morning.
Latest departure after tear down will be Monday morning.

Sanitary
--------

Our campground Welfenhof provides toilets.

Due to Corona we will probably build warm showers with *outdoor-experience*.

We will provide you with at least one place where you can get drinking water.

Hacking and Talks
-----------------

In the hack center, we solve exciting problems in close proximity.
Accordingly, we have decided that we do not want to offer a central hack center
this year.
Instead, we ask you to bring your own pavilions and seating and build
decentralized mini hack centers.

Additionally we'll bring our yurt as a platform for your talks and workshops.

Electricity
-----------

We will provide a basic electricity supply:
notebooks, gadgets and colourful light can be operated and charged.
Prepare for up to 50 m distance to the next outlet.
We calculate with a mean electrical energy demand below 100 W per hacker.
Should you plan to bring your electrically powered pizza ovens, water heaters,
whirlpools or similar appliances please contact us beforehand.

Communication
-------------

On the campsite LAN and WiFi are available.
We hope that we can connect our network to the Internet with sufficient bandwidth.
We will probably provide a DECT network.

FTTT - Fiber to the tent
------------------------

Besides classical copper-based network Fiber to the Tent will be available
this year.
By this free added-value service we will provide you with the usual performance
of the HOA network - but with the good feeling to have finally arrived in
the Cyber^WFiberspace.

PMR
---

For organisation of build-up and tear-down we will use Personal Mobile Radio
(PMR446).
For helpers we will bring a few spares.
If you have one by yourself: bring it.

Dogs
----

To make the Hacker Camping pleasant for as many hackers as possible we do not
allow dogs on the campsite.
Guide dogs and other assistance dogs are allowed.

Arrival
-------

.. raw:: html

   <p>
   <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=9.580078125%2C52.07022125944487%2C11.633148193359377%2C52.8525471567007&amp;layer=mapnik&amp;marker=52.46257%2C10.604559999999992" style="border: 1px solid black"></iframe><br/><small><a target="_blank" href="https://www.openstreetmap.org/?mlat=52.46257&amp;mlon=10.60456#map=10/52.46257/10.60456">Show bigger Map</a> <a href="geo:52.46257,10.60456">Geolink</a></small><br>
   The Hacken Open Air takes plact at the
   <a href="https://osm.org/go/0G7B2YRAL?m=" > Pfadfinderheim Welfenhof</a> in Gifhorn.
   Just 25 km north of Brunswick.
   </p>
   <p>
   The address for your navigation devices:<br>
   <address>
   III. Koppelweg 6<br>
   38518 Gifhorn
   </address>
   </p>
