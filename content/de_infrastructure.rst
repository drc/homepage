menu: Infos 
group: index
order: 10
template: base.html


Infos
=====

Deadlines
---------

T-Shirts
  Bestellungen müssen bis zum Freitag 20.08.2021 18:00
  durchgeführt **und** das Geld auf unserem Konto eingegangen sein.

Tickets stornieren
  Falls du dein Ticket zurückgeben
  möchtest musst du dies bis zum 22.08.2021 23:59 tun.
  Dies ist für dich kostenlos.

Tickets kaufen
  Aktuell gibt es für Tickets eine Warteliste.
  Falls Du noch Tickets kaufen möchtest trage dich dort ein.

Falls Du Fragen hast: kontakt@hackenopenair.de

Tickets
-------

Die Tickets vom HOA 2::20 sind dieses Jahr noch gültig.
Aktuell gibt es eine Warteliste.
Aufgrund der Corona-Maßnahmen wissen wir noch nicht, ob es einen weiteren
Ticketverkauf geben wird.

Falls Du dieses Jahr nicht am HOA teilnehmen kannst oder willst kannst du
dein Camping-Ticket noch bis zum 22.08.2021 stornieren und bekommst den
gesamten Ticketpreis zurück.
Jedes zurückgegebene Ticket wird danach an Personen in der Warteschlange
weitergegeben.

Im Eintritt sind enthalten:

* Eine coole Location zum Hacken und Chillen
* Die Möglichkeit an Vorträgen und Workshops teilzunehmen oder selber zu
  organisieren
* Frühstück am Frühstücksbuffet
* Warmes Abendessen (auch vegan)
* WLAN, Ethernet und DECT auf dem Campinggelände
* Camping mit deinem Zelt incl. Strom, warmer Dusche und Sanitäreinrichtungen

Zusätzlich gibt es vergünstigte Tickets für Junghacker zwischen 12 und 16 Jahren.

Die Teilnahme für Jüngsthacker ist frei.

In diesem Jahr wird es leider keine Tagestickets geben.

Wenn Du mit deinem Fahrzeug, Camper oder Wohnmobil auf dem Gelände campen möchtest
benötigst Du zusätzlich ein Fahrzeugticket.
Beachte: Die Anzahl der Fahrzeugtickets ist begrenzt.

In unserem Presale sind außerdem T-Shirts verfügbar.
Du kannst deine T-Shirts während der Veranstaltung abholen.
Für ein gemütliches Hacking-Wochenende stellen wir euch einiges an Infrastruktur
bereit.
Die folgenden Abschnitte sollen Dir einen Überblick geben, womit du rechnen
kannst.

Hygienekonzept
--------------

Wir möchten, dass du dich auf dem HOA sicher fühlen kannst.
Durch Corona wird das in diesem Jahr zu einer besonderen Herausforderung.
Zur Zeit entwickeln wir ein Hygienekonzept.
Falls Du jetzt schon einen Blick in dieses Dokument werfen möchtest findest
du es `hier </hygienekonzept.pdf>`__ zum Download.
Falls Du hierzu Feedback hast erreichst du uns unter *kontakt@hackenopenair.de*.

In ein paar Wochen wird es auch noch eine Zusammenfassung für Besucher geben.

Parken
------

Auf dem Gelände sind leider nur wenige Parkplätze vorhanden.
Wir klären aktuell noch verschiedene Varianten ab.
Wahrscheinlich werden wir aber auf Parkplätze im Ort ausweichen müssen.

An- und Abreise
---------------

Die reguläre Anreise ist am Donnerstag ab mittags.
Die reguläre Abreise ist am Sonntag nachmittags.

Wer uns beim Aufbauen helfen möchte kann ab Mittwoch morgens anreisen.
Die Abreise für Helfer ist spätestens am Montag Vormittag.

Sanitär
-------

Auf dem Welfenhof bei Gifhorn stehen uns feste Toiletten zur Verfügung.

Aufgrund der Corona-Situation wird es vermutlich wieder warme Duschen mit
*outdoor-experience* geben.

Zusätzlich wird es eine Stelle zur Trinkwasserversorgung geben.

Hacking und Vorträge
--------------------

Dank Corona werden in diesem Jahr kein zentrales Hackcenter haben!
Bringt also selber Pavillione und Sitzgelegentheiten mit und bildet dezentrale Mini-Hackcenter.
(Bei größeren Aufbauten gib uns bitte vorher Bescheid, damit wir diese einplanen
können.)

Zusätzlich bringen wir unsere Jurte als Plattform für deine Vorträge und
Workshops mit.

Strom
-----

Auf dem Platz wird es eine Strom-Grundversrogung geben.
Notebooks, Gadgets und buntes Licht können betrieben und geladen werden.
Rechne aber damit, dass der nächste Verteiler auch bis zu 50m von deinem Zelt entfernt
sein kann!
Wir rechnen pro Hacker mit einem mittleren Energiebedarf (aus Strom) von
unter 100 W.
Wenn Du also strombetriebene Pizza-Backöfen, Wasserkocher, Whirlpools oder
ähnliche Geräte mitbringen willst sprich das vorher mit uns ab.

Kommunikation
-------------

Auf dem Camp sind LAN und WLAN verfügbar.
Wir hoffen, dass wir unser Netzwerk mit ausreichender Bandbreite ans Internet
anbinden können.
Wahrscheinlich wird es auch DECT geben.

FTTT - Fiber to the tent
------------------------

Neben klassischem Kupfernetzwerk wird diesem Jahr auch Fiber to the Tent
verfügbar sein.
Bei diesem kostenlosen Mehrwertdienst bekommst du die gewohnte Leistung des
HOA-Netzwerkes mit dem guten Gefühl endlich im Cyber^WFiberspace angekommen
zu sein.

PMR
---

Zur Organisation von Auf- und Abbau werden wir PMR-Funk (PMR446) nutzen.
Wir werden eine kleine Anzahl Funkgeräte für Engel dabei haben.
Wenn du selber eins zur Verfügung hast: bring es mit.

Hunde
-----

Um für möglichst alle Teilnehmer eine angenehme Veranstaltung zu ermöglichen
sind Hunde auf dem Gelände nicht gestattet.
Blinden- und andere Diensthunde sind von dieser Regelung natürlich ausgenommen.

Anfahrt
-------

.. raw:: html

   <p>
   <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=9.580078125%2C52.07022125944487%2C11.633148193359377%2C52.8525471567007&amp;layer=mapnik&amp;marker=52.46257%2C10.604559999999992" style="border: 1px solid black"></iframe><br/><small><a target="_blank" href="https://www.openstreetmap.org/?mlat=52.46257&amp;mlon=10.60456#map=10/52.46257/10.60456">Größere Karte anzeigen</a> <a href="geo:52.46257,10.60456">Geolink</a></small><br>
   Das Hacken Open Air findet am <a href="https://osm.org/go/0G7B2YRAL?m=" >
   Pfadfinderheim Welfenhof</a> in Gifhorn
   - ca. 25km nördlich von Braunschweig - statt.
   </p>
   <p>
   Die Adresse für Navigationsgeräte ist:<br>
   <address>
   III. Koppelweg 6<br>
   38518 Gifhorn<br>
   </address>
   </p>
