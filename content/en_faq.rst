menu: FAQ
group: en
order: 1000
template: page.html


Frequently Asked Questions
==========================

* | **I have ordered a T-Shirt. Can you ship it to me?**
  | We cannot send your merch by parcel or letter. If you have ordered merch but are not at HOA, please ask a hacker you know to pick your order up for your.
