menu: Top
group: en
order: 1
template: page.html



About Hacken Open Air
=====================

Hacken Open Air is hacker-camping at it's finest.
The event is organized by the
Hackerspace `Stratum 0 <https://stratum0.org>`__.

The location for our cozy get-together will be
`Pfadfinderheim Welfenhof <https://osm.org/go/0G7B2YRAL?m=>`__
in Gifhorn, just 25km north of Brunswick.

Hacken Open Air is all about joint crafting, discussions and
barbecue. We deliberately forgo a lecture program. Instead we offer
you the space for self-organized lectures and workshops.

