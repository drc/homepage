menu: Updates
group: en
order: 999
template: page.html


Updates
-------

Update 2021-08-10: Ticket cancellation
======================================

If you can not or do not want to attend HOA this year you can cancel your
camping-ticket and get a 100% refund. This will be possible until 2021-08-22.
All tickets returned will be given to the people on the waiting list.

Update 2021-08-02: Tickets for HOA 2021
=======================================

Work on the hygiene concept is in full swing.
Therefore, we do not currently know how many additional tickets we can sell.
If you still want to buy a ticket for HOA 2021, add yourself to the waiting list
in our `Pretix <https://tickets.hackenopenair.de>`__.
As soon as tickets become available we will distribute them in the order of
registration via the waiting list.

Update 2021-07-28: We are GO for HOA 2021
=========================================


The HOA 2021 is getting closer: In merely six weeks we'll meet again!
To give you and us more planning security, we asked ourselves one last time:
Can and do we want to meet at HOA this year (\*0)?

This question is not easy: Of course we long for normality and for us too,
events like the HOA are part of that.
Nevertheless, we only want to go ahead if we consider the overall
corona-related health risk to be manageable.

Our answer to this questions is: The HOA will take place (\*0), but there will be some changes.
For the general hygiene concept, we're currently considering the following points:

* Vaccination:
  In our organization team, we will have a vaccination rate of almost 100% by the time HOA starts.
  It probably won't look much different for the rest of the Chaos  community.
  For everyone for whom there are no reasons against vaccination (medical, age, lack of vaccination recommendation),
  sufficient vaccination opportunities should now be available.
  (For those who are late to the game: there is still just enough time! ;-)
  We therefore think that it is in the spirit of solidarity towards all entities to get vaccinated (if possible).

  We expect two effects from a high vaccination rate:
  Infected people are less contagious and if we do have new infections at the event, the health effects are less serious.
  To confirm our assumption, there will be a survey in the Pretix for every ticket.
  We would like to ask everyone to take part.
  The deadline for the survey is 08/08/2021.

* No central hack center:
  In the hack center, we solve exciting problems in close proximity.
  Accordingly, we have decided that we do not want to offer a central hack center this year.
  Instead, we ask you to bring your own pavilions + seating and build decentralized mini hack centers.

* Further measures:
  We expect that there will be further measures in addition to the ones above.
  For example, more (outdoor) showers and DIXIs, mask requirements in certain situations
  (e.g. in the central building or during food distribution),
  daily rapid antigen self-tests for all participating (including vaccinated) entities or the like.


We have extended the return period for tickets to 2021-08-22.
If you cannot or do not want to take part in the HOA 2021, you can cancel your ticket in the Pretix.

When you originally purchased your ticket, you have given your arrival and departure details.
Please help us with the planning by updating these.
You can find all these questions on the details page of your ticket.
We emailed the link to all tickets again.


(\*0): In Lower Saxony, there is an official graduated plan that prohibits certain events beyond certain incidence levels.
If the incidence increases so much that we are affected, we of course have to cancel.
In this case we will refund all tickets and you will receive your t-shirts in a different way, e.g. by post.
