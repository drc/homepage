menu: Top
group: index
order: 1
template: page.html


Über das Hacken Open Air
========================

Das Hacken Open Air ist feinstes Hacker-Camping, ausgerichtet vom
Hackerspace `Stratum 0 <https://stratum0.org>`__.
Wir versammeln uns zum gemütlichen Treffen mit Freunden und Bekannten.

Beim Hacken Open Air geht es um gemeinsames Basteln, Diskutieren und
Grillen. Wir verzichten bewusst auf ein von uns organisiertes
Vortragsprogramm. Stattdessen lassen wir euch Freiraum für
selbstorganisierte Vorträge und Workshops.
