class Title:

    def contents_parsed(self, context):

        for content in context.contents:
            if content["content_title"] is not None and content["title"] is None:
                content["title"] = content["content_title"]
            elif content["title"] is not None and content["content_title"] is None:
                content["content_title"] = content["title"]
